import concurrent.futures
import contextlib
from dataclasses import dataclass
import logging
import pathlib
import sys
import typing
import warnings
import xml.etree.ElementTree as ET

import requests
import requests.exceptions
from requests.packages.urllib3.exceptions import InsecureRequestWarning


@contextlib.contextmanager
def _requests_no_insecure_warning():
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", category=InsecureRequestWarning)
        yield


LOG = logging.getLogger(__package__)


@dataclass
class Artifact:
    artifact_id: str
    group_id: str
    version: str
    uri: str

    @property
    def filename_from_uri(self):
        """Guess the name of the JAR based on the URI."""
        return self.uri.rsplit('/', 1)[1]


class CbngWebResolver(object):
    dependency_variable = '__cmmnbuild_deps__'
    description = 'CBNG Web Service'
    CBNG_WEB_ENDPOINT = 'https://acc-py-repo.cern.ch/cbng-web-resolver/resolve/productxml'

    @classmethod
    def is_available(cls):
        # check if CBNG web service is reachable
        try:
            with _requests_no_insecure_warning():
                requests.get(cls.CBNG_WEB_ENDPOINT, timeout=5.0, verify=False)
            return True
        except requests.exceptions.RequestException:
            try:
                import platform
                hostname = platform.node()
                if 'cern.ch' in hostname:
                    logging.warning(('This appears to be a CERN machine ({0}), but CBNG is not available. Will try ' +
                                     'to resolve dependencies from public repositories, which may not work for ' +
                                     'CERN internal modules.').format(hostname))
            except:
                pass
            return False

    def __init__(self, dependencies: typing.Iterable[str]):
        """Resolve dependencies using the CBNG web service"""
        valid_deps = self._get_valid_dependencies(dependencies)

        pxml = self.build_product_xml(valid_deps)

        # Post product.xml to CBNG web service
        LOG.info('resolving dependencies using CBNG web service')

        with _requests_no_insecure_warning():
            response = requests.post(
                self.CBNG_WEB_ENDPOINT, data=ET.tostring(pxml), verify=False,
            )
        #: The dependencies resolved by CBNG based on the set of input dependencies.
        self.resolved_artifacts = self._resolved_artifacts_from_response(
            response.json(),
        )

        LOG.info(
            f"CBNG resolved {len(self.resolved_artifacts)} artifacts: "
            f"{', '.join([dependency.artifact_id for dependency in self.resolved_artifacts])}"
        )

    def _resolved_artifacts_from_response(
            cls,
            resolved_deps_response: typing.List[typing.Dict[str, str]]
    ) -> typing.Tuple[Artifact]:
        result = []
        _response_to_kwarg_mapping = {
            'artifactId': 'artifact_id', 'groupId': 'group_id',
            'version': 'version', 'uri': 'uri',
        }
        for dependency in resolved_deps_response:
            artifact_kwargs = {}
            for field, kwarg_name in _response_to_kwarg_mapping.items():
                if field not in dependency:
                    raise ValueError(
                        f"Field '{field}' missing for dependency {dependency} in response:\n"
                        f"{resolved_deps_response}"
                    )
                artifact_kwargs[kwarg_name] = dependency[field]
            result.append(Artifact(**artifact_kwargs))
        return tuple(result)

    def _get_valid_dependencies(self, dependencies):
        valid_deps = []
        for dep in dependencies:
            missing_msg = f'GroupId for {dep} is missing. cbng requires this information from Feb 2021'
            if isinstance(dep, str):
                LOG.warning(missing_msg)
                valid_deps.append({'product': dep})
            elif isinstance(dep, dict):
                if 'groupId' not in dep:
                    LOG.warning(missing_msg)
                valid_deps.append({str(k): str(v) for k, v in dep.items()})
            else:
                LOG.warning(f'Ignoring "{dep}" - __cmmnbuild_deps__ must be a list of str or dict')
        return valid_deps

    @classmethod
    def build_product_xml(cls, valid_deps):
        # Generate product.xml
        cmmnbuild_dep_mgr = __package__.split('.')[0]
        pxml = ET.Element('products')
        pxml_prod = ET.SubElement(pxml, 'product', attrib={
            'name': cmmnbuild_dep_mgr,
            'version': sys.modules[cmmnbuild_dep_mgr].__version__,
            'directory': cmmnbuild_dep_mgr
        })
        pxml_deps = ET.SubElement(pxml_prod, 'dependencies')
        for dep in valid_deps:
            ET.SubElement(pxml_deps, 'dep', attrib=dep)
        return pxml

    def save_jars(self, dir):
        """Save the resolved artifacts to the given directory, removing any pre-existing JARs."""
        # Save metadata about resolved packages
        # json.dump(self.resolved_deps, open(os.path.join(dir, "dependencies.json"), "w"))
        self.download_jars(pathlib.Path(dir), self.resolved_artifacts)

    @classmethod
    def _download_jar(cls, lib_dir: pathlib.Path, dep: Artifact):
        # Function to download a single jar into a given directory
        with _requests_no_insecure_warning():
            jar_request = requests.get(dep.uri, stream=True, timeout=20.0, verify=False)
        LOG.info('downloading {group}:{artifact}:{version}'.format(
            group=dep.group_id, artifact=dep.artifact_id, version=dep.version
        ))
        with (lib_dir / dep.filename_from_uri).open("wb") as jar_file:
            for chunk in jar_request.iter_content(chunk_size=1024):
                jar_file.write(chunk)

    @classmethod
    def download_jars(cls, directory: pathlib.Path, artifacts: typing.Tuple[Artifact]):
        """Download the given :class:`Artifact` instances into the given
        directory, removing any pre-existing JARs in the directory if necessary.

        """
        existing_jars = {jarfile.name for jarfile in directory.glob('*.jar')}
        desired_jars = {artifact.filename_from_uri for artifact in artifacts}

        jars_to_remove = existing_jars - desired_jars
        if jars_to_remove:
            LOG.info(f'removing {len(jars_to_remove)} existing jars')

        jars_to_keep = existing_jars & desired_jars
        if jars_to_keep:
            LOG.info(f"{len(jars_to_keep)} of {len(desired_jars)} JARs already downloaded")

        with concurrent.futures.ThreadPoolExecutor(6) as executor:
            # Create a pool of threads for parallel downloading
            download_tasks = {
                executor.submit(cls._download_jar, directory, artifact): artifact.uri
                for artifact in artifacts
                if artifact.filename_from_uri not in existing_jars
            }

            # Meanwhile, remove the existing JARs that we don't want.
            for jar_to_remove in jars_to_remove:
                (directory / jar_to_remove).unlink()

            # Wait for the downloads to complete and check results are exceptions
            # (potentially raising if they are).
            for task in concurrent.futures.as_completed(download_tasks):
                task.result()

    @classmethod
    def get_help(cls, classnames, class_info):
        for classname in classnames:
            lst = classname.split('.')
            data = {'u': 'https://artifactory.cern.ch/development',
                    'r': '/'.join(lst),
                    'o': lst[0],
                    'a': lst[1]}
            for package, version in class_info[classname]:
                print('Info for "{0}" in "{1}" version "{2}"'.format(
                    classname, package, version
                ))
                data['v'] = version
                data['p'] = package
                url = ('JavaDoc: {u}/{o}/{a}/{p}/{v}/{p}-{v}-javadoc.jar'
                       '!/index.html?{r}.html')
                print(url.format(**data))
                url = ('Sources: {u}/{o}/{a}/{p}/{v}/{p}-{v}-sources.jar'
                       '!/{r}.java')
                print(url.format(**data))
