import typing

import pytest

from cmmnbuild_dep_manager.resolver.cbng_web import CbngWebResolver, Artifact


def mock_resolver(resolved_artifacts: typing.List[Artifact]):
    class TestCbngWebResolver(CbngWebResolver):
        def __init__(self):
            self.resolved_artifacts = resolved_artifacts

        @classmethod
        def _download_jar(cls, lib_dir, dep):
            jar_filename = dep.uri.split('/')[-1]
            create_mock_jar_file(lib_dir, jar_filename)

    return TestCbngWebResolver()


@pytest.fixture
def resolver_w_2_artifacts():
    unused_kwargs = {'artifact_id': '', 'group_id': '', 'version': ''}
    resolver = mock_resolver([
        Artifact(uri='www.some.where/group/atr/todownload.jar', **unused_kwargs),
        Artifact(uri='www.some.where/group/atr/existing.jar', **unused_kwargs),
    ])
    return resolver


def create_mock_jar_file(lib_dir, filename, content=""):
    jar_file_path = lib_dir / filename
    jar_file_path.write_text(content)


def test_resolve_and_download(tmp_lib_dir, local_cbng_web_endpoint):
    resolver = CbngWebResolver(['japc'])
    resolver.save_jars(tmp_lib_dir)

    for dep in resolver.resolved_artifacts:
        jar_file = tmp_lib_dir / dep.filename_from_uri
        assert jar_file.exists()
        # Check if file is not empty
        assert jar_file.stat().st_size > 0


def test_resolver_download_all_jars_when_libs_empty(tmp_lib_dir, resolver_w_2_artifacts):
    resolver_w_2_artifacts.save_jars(tmp_lib_dir)
    assert (tmp_lib_dir / "todownload.jar").exists()


def test_resolver_download_only_missing_jars(tmp_lib_dir, resolver_w_2_artifacts):
    create_mock_jar_file(tmp_lib_dir, "existing.jar", content="jar_content")
    resolver_w_2_artifacts.save_jars(tmp_lib_dir)
    # Check previously existing file content wasn't edited
    assert (tmp_lib_dir/"existing.jar").read_text() == 'jar_content'


def test_resolver_removes_unnecessary_jars(tmp_lib_dir, resolver_w_2_artifacts):
    create_mock_jar_file(tmp_lib_dir, "unwanted.jar")
    resolver_w_2_artifacts.save_jars(tmp_lib_dir)
    assert not (tmp_lib_dir / "unwanted.jar").exists()
