import os
import signal
import subprocess
import sys
import time
from unittest.mock import patch
import urllib.request
import urllib.error

import pytest


def poll_endpoint_return_code(url):
    while True:
        try:
            conn = urllib.request.urlopen(url)
            code = conn.getcode()
        except urllib.error.URLError:
            code = 500
        yield code


@pytest.fixture(scope="session")
def cbng_web_server(tmpdir_factory):
    tmpdir = tmpdir_factory.mktemp('data')

    proc = subprocess.Popen(
        [
            sys.executable,
            '-m', 'cbng_web_service'
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        env=dict(os.environ, **{
            'CBNG_WEB_SERVICE_LOGS_DIR': tmpdir,
            'CBNG_WEB_SERVICE_WORKERS': "2",
            'CBNG_WEB_SERVICE_PORT': "8089",
        }),
        preexec_fn=os.setsid,
    )

    # Give the server time to start
    for code in poll_endpoint_return_code('http://localhost:8089/resolve/productxml'):
        if code == 200:
            break
        time.sleep(1)

    # Check it started successfully
    assert not proc.poll(), proc.stdout.read().decode("utf-8")
    yield proc

    # Shut it down at the end of the pytest session.
    os.killpg(os.getpgid(proc.pid), signal.SIGTERM)


@pytest.fixture
def local_cbng_web_endpoint(cbng_web_server):
    resolve_endpoint = 'http://localhost:8089/resolve/productxml'
    with patch('cmmnbuild_dep_manager.resolver.cbng_web.CbngWebResolver.CBNG_WEB_ENDPOINT',
               resolve_endpoint):
        yield
