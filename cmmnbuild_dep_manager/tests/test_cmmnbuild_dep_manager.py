from contextlib import contextmanager
import logging
import types
import unittest.mock
import sys

import pytest

from cmmnbuild_dep_manager.resolver import Resolver
from .test_Manager__load_modules import tmp_modules_content, FakePkgMgr


class SimpleResolver(Resolver):
    dependency_variable = '__test_resolver_deps__'
    description = 'The test resolver which does nothing'

    @classmethod
    def is_available(cls):
        return True

    def __init__(self, dependencies):
        pass

    def save_jars(self, dir):
        pass

    @classmethod
    def get_help(cls, classnames, class_info):
        raise ValueError('No help available from this testable resolver')


@pytest.fixture
def simple_resolver():
    with unittest.mock.patch(
            'cmmnbuild_dep_manager.resolver.resolvers',
            return_value=[SimpleResolver]):
        yield


@contextmanager
def tmp_mod(name, dependencies=None, version=None):
    """
    Generate a temporary module that is suitable for testing dependency
    resolution with.

    """
    mod = types.ModuleType('a_cmmnbuild_test_module')
    if dependencies is not None:
        mod.__test_resolver_deps__ = dependencies
    if version is not None:
        mod.__version__ = version
    sys.modules[name] = mod

    try:
        yield mod
    finally:
        sys.modules.pop(name)


def test_no_such_module(caplog, manager_cls, simple_resolver):
    manager_cls('this_mod_doesnt_exist')
    assert caplog.record_tuples == [
        ("cmmnbuild_dep_manager", logging.INFO,
         'Package "this_mod_doesnt_exist" is not yet set up - installing and resolving JARs'),
        ("cmmnbuild_dep_manager", logging.ERROR,
         'this_mod_doesnt_exist not found'),
    ]


def test_module_missing_special_resolver_attr(caplog, manager_cls, simple_resolver):
    with tmp_mod('a_cmmnbuild_test_module', version='1.2.3'):
        manager_cls('a_cmmnbuild_test_module')

    warning = (
        "cmmnbuild_dep_manager", logging.ERROR,
        'module a_cmmnbuild_test_module does not declare '
        '__test_resolver_deps__, which is needed for SimpleResolver '
        '(The test resolver which does nothing) ')
    assert warning in caplog.record_tuples


def test_module_dependencies_no_version(caplog, manager_cls, simple_resolver):
    with tmp_mod('a_cmmnbuild_test_module', dependencies=['j1', 'j2']):
        manager_cls('a_cmmnbuild_test_module')
    warning = (
        "cmmnbuild_dep_manager", logging.ERROR,
        "module 'a_cmmnbuild_test_module' has no attribute '__version__'")
    assert warning in caplog.record_tuples


@pytest.fixture
def mocked_mgr(manager_cls):
    """
    A fixture that provides a clean manager and resolver for mocked testing of
    module installation.

    """
    mock_resolver = unittest.mock.Mock(
        __name__='mock_resolver',
        dependency_variable='__test_resolver_deps__',
        description="the mock resolver",
    )

    mock_resolvers = unittest.mock.patch(
        'cmmnbuild_dep_manager.resolver.resolvers',
        return_value=[mock_resolver])

    resolver = mock_resolver
    with mock_resolvers:
        yield manager_cls(), resolver


def test_module_dependencies(mocked_mgr):
    mgr, m_resolver = mocked_mgr

    mod1 = tmp_mod(
        'mod1', version='1.2.3', dependencies=['java_dep1', 'java_dep2'])

    with mod1:
        mgr.install('mod1')

    m_resolver.assert_called_once_with(
        ['java_dep1', 'java_dep2'])


def test_multi_module_dependencies(mocked_mgr):
    mgr, m_resolver = mocked_mgr

    mod1 = tmp_mod(
        'mod1', version='1.2.3', dependencies=['java_dep1', 'java_dep2'])
    mod2 = tmp_mod(
        'mod2', version='1.2.3', dependencies=['java_dep2', 'java_dep3'])

    with mod1, mod2:
        mgr.install('mod1')
        m_resolver.reset_mock()
        mgr.install('mod2')

    m_resolver.assert_called_once_with(
        ['java_dep1', 'java_dep2', 'java_dep2', 'java_dep3'])


def test_jarversion(mocked_mgr):
    mgr, m_resolver = mocked_mgr

    assert mgr._get_jarversion('/a/long/path/library-1.2.3.jar') == ('library', '1.2.3')
    assert mgr._get_jarversion('/a/long/path/lib-rary-1.2.3.jar') == ('lib-rary', '1.2.3')
    assert mgr._get_jarversion('lib-rary-1.2.3-SOMETHING.jar') == ('lib-rary', '1.2.3-SOMETHING')
    assert mgr._get_jarversion('library-1.2.3-SOME-THING.jar') == ('library', '1.2.3-SOME-THING')
    assert mgr._get_jarversion('1.2.3-4.5.6-SOME-THING.jar') == ('1.2.3', '4.5.6-SOME-THING')
    
    # no version number
    with pytest.raises(ValueError):
        mgr._get_jarversion('library.jar')


def test_class_path_join(mocked_mgr):
    mgr = mocked_mgr[0]
    extras = ['module1.jar', 'path/to/mod2.jar']
    with unittest.mock.patch('os.name', 'nt'):
        assert mgr.class_path(extras) == "module1.jar;path/to/mod2.jar"

    with unittest.mock.patch('os.name', 'something-else'):
        assert mgr.class_path(extras) == "module1.jar:path/to/mod2.jar"


def test_imports_removed_on_success(mocked_mgr):
    pytest.importorskip('jpype', minversion='1.0')
    mgr = mocked_mgr[0]
    with mgr.imports():
        import java.util
        assert java.util in sys.modules.values()
    assert java.util not in sys.modules.values()


def test_imports_removed_on_failure(mocked_mgr):
    pytest.importorskip('jpype', minversion='1.0')
    mgr = mocked_mgr[0]
    with pytest.raises(ValueError):
        with mgr.imports():
            import java.util
            assert java.util in sys.modules.values()
            raise ValueError()
    assert java.util not in sys.modules.values()


def test_imports_are_reentrant(mocked_mgr):
    pytest.importorskip('jpype', minversion='1.0')
    mgr = mocked_mgr[0]
    with mgr.imports():
        import java.util
        with mgr.imports():
            import java.lang
            assert java.util in sys.modules.values()
            assert java.lang in sys.modules.values()
        assert java.util in sys.modules.values()
        assert java.lang not in sys.modules.values()
    assert java.util not in sys.modules.values()
    assert java.lang not in sys.modules.values()


def test_init_manager_no_ping_cbng_web(manager_cls):
    # We shouldn't need cbng-web to be up in order to initialise a manager for
    # an already resolved environment.
    assert manager_cls()


def test_init_manager_no_ping_cbng_web_with_package_name_already_resolved(
        manager_cls):
    # We shouldn't need cbng-web to be up in order to initialise a manager for
    # an already resolved environment.
    with FakePkgMgr.fake_pkg("fake_cmmnbuild_pkg", "1.2.3", entrypoint="1.2.3"):
        with tmp_modules_content({'fake_cmmnbuild_pkg': '1.2.3'}, manager_cls.test_modules_json):
            assert manager_cls('fake_cmmnbuild_pkg')


def test_resolve_manager_pings_cbng_web(manager_cls):
    # Validate that resolving does indeed hit the cbng-web endpoint,
    # and that the no_cbng_web_ping_allowed fixture is doing the right thing.
    with pytest.raises(RuntimeError):
        assert manager_cls().resolve()
