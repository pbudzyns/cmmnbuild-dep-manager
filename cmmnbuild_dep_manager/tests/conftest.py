from unittest.mock import patch

import pytest

import cmmnbuild_dep_manager as cbdm


@pytest.fixture(scope='function')
def manager_cls(tmp_lib_dir):
    class TestManager(cbdm.Manager):
        test_modules_json = tmp_lib_dir.parent / 'modules.json'

        def jar_path(self):
            return tmp_lib_dir

        def _user_dir(self):
            return tmp_lib_dir.parent

        def _dist_dir(self):
            return tmp_lib_dir.parent

    with patch('cmmnbuild_dep_manager.cmmnbuild_dep_manager.PKG_MODULES_JSON', tmp_lib_dir.parent):
        yield TestManager


@pytest.fixture(scope='session', autouse=True)
def no_cbng_web_ping_allowed():
    with patch('cmmnbuild_dep_manager.resolver.cbng_web.CbngWebResolver.is_available',
        side_effect=RuntimeError('The is_available method was called on the CbngWebResolver')
    ):
        yield


@pytest.fixture(scope='function')
def tmp_lib_dir(tmp_path):
    tmp_dir = tmp_path / "lib"
    tmp_dir.mkdir()
    return tmp_dir
